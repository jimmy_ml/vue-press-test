module.exports = {
  base: '/vue-press-test/',
  title: 'my document',
  dest: 'public',
  description: 'documents library',
  themeConfig: {
    repo: 'https://gitlab.com/jimmy_ml/vue-press-test/',
    docsDir: 'docs',
    nav: [
      {
        text: 'about',
        link: '/about/',
      }
    ]
  }
};